<? $h1 = "Grade de proteção nr12";
$title  = "Grade de proteção nr12";
$desc = "Se está procurando ofertas de $h1, você adquire no website Soluções Industriais, cote produtos já com aproximadamente 100 fabricantes ao mesmo tempo";
$key  = "Grades de proteção nr12,proteção nr12";
include('inc/head.php');
include('inc/fancy.php'); ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/grade-de-protecao-nr12-01.jpg" title="<?= $h1 ?>" class="lightbox"><img class="lazyload" data-src="<?= $url ?>imagens/mpi/thumbs/grade-de-protecao-nr12-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/grade-de-protecao-nr12-02.jpg" title="Grades de proteção nr12" class="lightbox"><img class="lazyload" data-src="<?= $url ?>imagens/mpi/thumbs/grade-de-protecao-nr12-02.jpg" title="Grades de proteção nr12" alt="Grades de proteção nr12"></a><a href="<?= $url ?>imagens/mpi/grade-de-protecao-nr12-03.jpg" title="proteção nr12" class="lightbox"><img class="lazyload" data-src="<?= $url ?>imagens/mpi/thumbs/grade-de-protecao-nr12-03.jpg" title="proteção nr12" alt="proteção nr12"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>Se você procura um sistema de proteção modular e flexível, a solução é a <strong>grade de proteção NR12</strong> TROAX. Os sistemas de proteção física representam aproximadamente 60% das necessidades de adequação a NR12.</p>
                        <p>Com o uso da grade de proteção adequada à NR12, garante-se:</p>
                        <ul class="topicos-relacionados">
                            <li>Segurança de bens;</li>
                            <li class="li-mpi">Segurança de processos;</li>
                            <li class="li-mpi">Segurança de pessoas.</li>
                        </ul>
                        <p>A grade para proteção NR12 apresenta resistência para impactos de 309 até 1.600 Joules, exigência feita pelas principais montadoras alemãs.</p>
                        <h2>Características do produto</h2>
                        <p>A <strong>grade de proteção NR12</strong> apresenta uma série de características, tais como:</p>
                        <ul class="topicos-relacionados">
                            <li>É modular;</li>
                            <li class="li-mpi">Utiliza como altura padrão dos painéis 2050 mm e estes são instalados juntamente com os postes para fixação;</li>
                            <li class="li-mpi">Os painéis são instalados a uma distância de 150 mm do chão;</li>
                            <li class="li-mpi">A altura pode ser facilmente ajustada com a utilização do "Kit Cut" (perfil de corte para adaptação horizontal);</li>
                            <li class="li-mpi">A largura da grade de proteção tem 07 padrões: 200 mm; 300 mm; 700 mm; 800 mm; 1.000 mm; 1.200mm e 1.500 mm podendo esta ser ajustadas de acordo com suas necessidades e com a utilização do "Kit End" (perfil de corte para adaptação vertical). Ambas as adaptações são feitas sem a necessidade de solda.</li>
                            <li class="li-mpi">Pode-se combinar painéis para atingir maiores larguras sem a utilização de postes com o "Snapper" (conector de reforço).</li>
                        </ul>
                        <h2>Informações adicionais</h2>
                        <p>Por ser uma grade extremamente modular e flexível, são diversas as opções de largura da grade de proteção TROAX, como por exemplo: 200 mm, 300 mm, 700 mm, 800 mm, 1.000 mm, 1.200 mm, 1.500 mm e estas também são ajustáveis através do "kit end" que é o kit para adaptações verticais, que também é feita sem necessidade de solda. Para medidas maiores que 1.500mm sem a utilização de postes utiliza-se o "Snapper" que atua como um conector de reforço para fixação dos painéis.</p>
                        <h2>O que são as grades de proteção?</h2>
                        <p>As grades de proteção são estruturas metálicas que são instaladas em janelas, sacadas, escadas, entre outros espaços, com o objetivo de garantir a segurança das pessoas em relação a quedas e acidentes. São especialmente importantes em ambientes com crianças, idosos ou pessoas com deficiência, que podem estar mais suscetíveis a acidentes.</p>
                        <h2>Por que instalar grades de proteção?</h2>
                        <p>A instalação de grades de proteção é importante para garantir a segurança das pessoas em relação a quedas e acidentes. Além disso, em muitas cidades, a instalação de grades de proteção é obrigatória por lei, especialmente em prédios e edifícios com mais de um andar. As grades de proteção podem ser fabricadas em diferentes materiais e modelos, adaptando-se às necessidades de cada espaço e ambiente.</p>

                        <h2>Como escolher as grades de proteção adequadas?</h2>
                        <p>Ao escolher as grades de proteção, é importante levar em consideração o ambiente em que serão instaladas, bem como o material, o modelo e a qualidade da estrutura. As grades de proteção devem ser fabricadas com materiais resistentes e duráveis, como o aço carbono ou inox, e devem ser instaladas por profissionais qualificados. É importante garantir que as grades de proteção estejam de acordo com as normas de segurança, como as especificações da ABNT, para garantir a proteção e segurança das pessoas.</p>
                        <h2>Sobre a empresa</h2>
                        <p>A empresa VECSA fornece soluções para proteção de máquinas, robôs e perímetros. Utilizando um sistema de painéis modular, conseguimos atender às mais variadas necessidades de nossos clientes.</p>
                        <p>Para obter maiores informações sobre a <strong>grade de proteção NR12,</strong> entre em contato com a empresa! Aproveite para solicitar um orçamento!</p>
                        <p>Todos os produtos possuem norma ISO Standards de qualidade.</p>
                        <p>Faça um orçamento pelo formulário abaixo. É simples, rápido e gratuito!</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>