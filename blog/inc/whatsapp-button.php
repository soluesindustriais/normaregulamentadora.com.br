<?php
$wppNumbers = [
	[
		'number' => $whatsapp,
		'title' => $nomeSite,
	],
];
?>
<div class="wpp-container">
	<?php foreach ($wppNumbers as $wppItem => $wppValue) : ?>
		<div data-button="<?= $wppItem ?>" class="wppButton whatsapp">
			<img class="wppButton__image" src="<?=$url?>imagens/icones/whatsapp-icon.svg" alt="WhatsApp <?= $wppValue['title'] ? $wppValue['title'] : $nomeSite ?>" title="WhatsApp <?= $wppValue['title'] ? $wppValue['title'] : $nomeSite ?>" width="24" height="24">
		</div>
		<div data-popup="<?= $wppItem ?>" class="wppPopup">
			<div class="wppPopup__inner">
				<div class="wppPopup__head">
					<div class="wppPopup__icon">
						<i class="fab fa-whatsapp fa-4x"></i>
					</div>
					<div class="wppPopup__info">
						<span><strong><?= $wppValue['title'] ? $wppValue['title'] : 'Comercial ' . $nomeSite ?></strong></span>
						<span><?= $_SERVER['HTTP_HOST'] ?></span>
						<span>Online</span>
					</div>
				</div>
				<div class="wppPopup__body">
					<span class="wppPopup__label">Fale com a gente pelo WhatsApp</span>

					<form data-form="<?= $wppItem ?>" class="wppPopup__form" enctype="multipart/form-data" method="post">
						<input class="wppNumber" type="hidden" name='whatsapp' value="<?= $wppValue['number'] ?>">

						<div class="wppPopup__form-input">
							<span class="icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
									<path d="M12 4a4 4 0 1 1 0 8 4 4 0 0 1 0-8zm0 10c4.418 0 8 1.79 8 4v2H4v-2c0-2.21 3.582-4 8-4z" fill="#57b846"></path>
								</svg></span>
							<input class="wppName" type="text" name="nome" placeholder="Nome completo *" value="">
						</div>

						<div class="wppPopup__form-input">
							<span class="icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="18" height="18" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
									<path d="M6.623 10.793a15.068 15.068 0 0 0 6.589 6.585l2.2-2.203c.275-.275.67-.356 1.016-.245 1.124.367 2.325.568 3.572.568a1 1 0 0 1 1 1v3.5a1 1 0 0 1-1 1c-9.39 0-17.001-7.611-17.001-17a1 1 0 0 1 1-1h3.5a1 1 0 0 1 1 1c0 1.247.2 2.448.568 3.572a1 1 0 0 1-.244 1.015l-2.2 2.208z" fill="#57b846"></path>
								</svg></span>
							<input class="wppPhone" type="text" name="telefone" placeholder="DDD + Celular *" value="">
						</div>

						<input type="hidden" name="email" value="naoinformado@naoinformado.com">
						<input type="hidden" name="como_nos_conheceu" value="Não informado">
						<input type="hidden" name="mensagem" value="Não informado">

						<div class="g-recaptcha" data-size="<?= (!$isMobile) ? 'normal' : 'compact' ?>" data-sitekey="<?= $siteKey; ?>"></div>
						<div class="wppPopup__form-submit">
							<button data-analytics type="submit" name="EnviaWhatsApp" title="Enviar">Enviar &nbsp;<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="20" height="20" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24">
								<path d="M2 21l21-9L2 3v7l15 2l-15 2v7z" fill="#57b846"></path>
							</svg><span></span></button>
						</div>
					</form>
				</div>
				<div class="wppClose">
					<span>&times;</span>
				</div>
			</div>
		</div>
			
	<?php endforeach ?>
</div>