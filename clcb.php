<? $h1 = "Clcb";
$title  = "Clcb";
$desc = "Se pesquisa por $h1, encontre as melhores indústrias, faça uma cotação já com aproximadamente 200 indústrias"; $key  = "laudo Clcb,Empresa de Clcb";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/clcb-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/clcb-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/clcb-02.jpg" title="laudo Clcb" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/clcb-02.jpg" title="laudo Clcb" alt="laudo Clcb"></a><a href="<?=$url?>imagens/mpi/clcb-03.jpg" title="Empresa de Clcb" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/clcb-03.jpg" title="Empresa de Clcb" alt="Empresa de Clcb"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
					<h2>CLCB: CERTIFICADO DE LICENÇA DO CORPO DE BOMBEIROS</h2>
					<p>Todos os estabelecimentos sejam eles residenciais ou comerciais é preciso que sigam algumas exigências estabelecidas por alguns órgãos como prefeitura e Corpo de bombeiros. O único modelo de imóvel que é isento de algumas exigências são os que habitam uma única família e são considerados como unifamiliares.</p>
					<p>Muitos pensam que os bombeiros agem apenas no combate ao incêndio, em grande parte do tempo sim, mas existem outras ações que são responsáveis, como exemplo, a fiscalização de imóveis a fim de averiguar se o imóvel atende às exigências de segurança estabelecidas.</p>
					<p>O corpo de bombeiros também agem também nas especificações da norma, pois há uma normativa que considera todo os itens de segurança e suas exigências, além de existir algumas particularidades a serem seguidas pelos bombeiros municipais, pois as formas de atendimento acontecem de forma diferenciada em cada região.</p>
					<p>Esses procedimentos existem a fim de obter o <strong>CLCB</strong> (Certificado de Licença do Corpo de Bombeiros) ou também conhecido como AVCB (Auto de vistoria do Corpo de Bombeiros), ambos refere-se ao mesmo documento, ocorre que em algumas localidades as nomenclaturas são diferenciadas.</p>
					<p>Este documento tem como objetivo regularizar o imóvel perante ao órgão, pois representa que o bombeiro vistoriou o local e aprovou a vistoria atestando que os local tem condições básicas contra incêndio. Mas para obtenção desse documento é necessário que um projeto seja elaborado.</p>

					<p>Veja também <a href="https://www.normaregulamentadora.com.br/emissao-de-avcb-sp" style="cursor: pointer; color: #006fe6;font-weight:bold;">Emissão de AVCB SP</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>

					<h2>COMO ELABORAR O PROJETO DE INCÊNDIO E QUAIS SUAS CARACTERÍSTICAS?</h2>
					<p>O primeiro passo para a elaboração do projeto de incêndio é a planta arquitetônica, pois refere-se a uma base estrutural que representa o imóvel e suas medidas, considerando janelas, portas, escadas, metragem, especificação de cômodos, entre outras informações arquitetônicas que são levantadas neste documento.</p>
					<p>Após se basear no projeto arquitetônico, é feita a contratação de um engenheiro e um profissional especialista, pois este deve analisar através do primeiro projeto as características do ambiente para levantar quais são as normas e assim desenvolve o PPCI  (Projeto de combate ao Incêndio).</p>
					<p>A partir da avaliação do ambiente e o levantamento da norma para determinada localidade, é possível  estabelecer quais os equipamentos e características para determinado ambiente, com isso, fica mais fácil elaborar após esses levantamentos e definições, lembrando que devem ser desenvolvidas considerando as normas estabelecidas, pois existem algumas particularidades e é preciso que o profissional esteja atento no momento da criação do projeto.</p>
					<p>Também é importante que o profissional seja cadastrado no CREA (Conselho Federal  de Engenharia e Agronomia) ou no CAU (Conselho de Arquitetura e Urbanismo), pois estes cadastros garante que o profissional possui competência para  criação do projeto, podendo também assiná-lo como responsável técnico.</p>
					<p>No caso do estado de São Paulo, o <strong>CLCB</strong> somente é exigido para imóveis com metragem superior a 750 metros e com volume de circulação menor que 250 pessoas, sendo assim, imóveis com características inferior não possuem obrigação em obter o <strong>CLCB</strong>. Por isso, mencionamos que é importante que o profissional verifique as particularidades que a norma estipula para cada região e cada tipo de imóvel.</p>
					<h2>PROCESSO DE APROVAÇÃO DO PROJETO</h2>
					<p>Assim que a planta com todas as especificações, considerando quantidade e tipos de equipamentos for finalizada, é importante que alguns outros documentos acompanhem a planta no momento da apresentação no Corpo de Bombeiro local. Veja abaixo os documentos:</p>
					<ul class="topicos-relacionados">
						<li><strong>Planta de Incêndio:</strong> É integrada por símbolos que representam os equipamentos de incêndio e são descritos através de uma legenda;</li>
						<li><strong>Memorial descritivo:</strong> É um material que descreve os equipamentos e o local de forma mais detalhada que o projeto;</li>
						<li><strong>A anotação de responsabilidade técnica:</strong> é assinado por um técnico que o responsabiliza sobre determinada obra ou item especificado no documento;</li>
						<li><strong>Taxa de análise de projeto (DAE):</strong> refere-se a uma taxa estadual que é paga para que o processo de análise seja iniciado no Corpo de Bombeiros.</li>
					</ul>
					<p>Assim que todos forem obtidos, é necessário que um requerente compareça ao Corpo de bombeiro para apresentação da documentação. Os bombeiros analisam dentro do período médio de 30 dias, porém esse prazo pode variar de acordo com determinada região. Lembrando que o processo costuma ser iniciado apenas quando a taxa é paga e o órgão costuma liberá-la no ato do protocolo do projeto.</p>
					<p>Após o pagamento, o projeto é analisado e o bombeiro vai analisar se o ambiente e suas características e se os equipamentos e quantidades mencionadas conseguem proporcionar um ambiente mais seguro. Em São Paulo, o resultado da análise costuma ser emitido em 30 dias após o pagamento da taxa e esse processo pode ser acompanhado pela internet através do site pelo número de protocolo ou pode ser feito presencialmente.</p>
					<p>Existem duas possibilidades após a análise, o projeto pode ser aprovado ou reprovado, portanto, sse aprovado significa que a obr proposta pode ser executada, mas se reprovada essa informação virá acompanhada de um relatório especificando o motivo e quais as adequações devem ser feitas no projeto para que seja reapresentado.</p>
					<p>No caso do projeto reprovado, esse procedimento deve se repetir até que o projeto seja aprovado. Para evitar que o projeto seja reprovado é fundamental que as exigências da norma sejam seguidas, pois assim diminui as chances de apontamentos.</p>
					<h2>EXECUÇÃO DA OBRA</h2>
					<p>Quando finalmente o projeto for aprovado significa que a execução da obra foi liberada, sendo assim, é necessário contratar funcionários que sejam especialistas em equipamentos de incêndio para que a instalação desses itens sejam feitos da forma correta. Um ponto muito importante nesse momento é considerar todos os pontos do projeto aprovado no momento  execução.</p>
					<p>Assim que a obra for finalizada o requerente deve retornar ao Corpo de Bombeiros a fim do  imóvel receber a vistoria dos bombeiros que vão fiscalizar e comparar se o local atende as exigências e se a obra condiz com o projeto que foi aprovado. Por isso, é importante muita atenção no momento de execução, pois antes da emissão do <strong>CLCB</strong> os bombeiros fiscalizam o local, a fim de conferir se os equipamentos de incêndios oferecem segurança e o devido suporte em caso de uma situação de risco.</p>
					<p>Se a vistoria for aprovada, o <strong>CLCB</strong> é emitido após alguns dias e retirado pessoalmente ou pela internet, mas caso a vistoria seja reprovada o bombeiro vai emitir um relatório com todos os pontos a ajustar no local e os equipamentos terão que ser ajustados conforme os apontamentos feitos.</p>
					<p>Assim que as adequações forem finalizadas, no caso de vistoria reprovada, o requerente deve acionar novamente os bombeiros para retornarem no local e averiguar se os equipamentos atendem as exigências. Sendo assim, esse procedimento se repete até que o <strong>CLCB</strong> seja emitido.</p>
					<p>Para que não ocorra muitas adequações, pois os gastos são consideráveis é preciso que o projeto seja seguido a risca e atenda todas as exigências, com isso, as chances de reprovação diminuem. Um ponto muito importante para enfatizar refere-se que a aprovação do projeto não licencia o imóvel, pois muitas pessoas consideram que após a aprovação do projeto o imóvel está regular, porém somente após a vistoria e emissão do CLBC que a edificação é considerada regular no quesito e segurança contra incêndio.</p>
					<h2>QUAL A RELEVÂNCIA DO CLCB?</h2>
					<p>Este documento conhecido como Certificado de licença do Corpo de bombeiros e é responsável pela regularização do imóvel com o Corpo de Bombeiros e quando é emitido significa que todas as etapas do processo de regularização foram cumpridas e que o local apresenta as condições de segurança para situações de risco.</p>
					<p>Mesmo que o documento esteja emitido pelo órgão, o proprietário do local continua sendo responsável, pois é dever do responsável manter os equipamentos de incêndio em funcionamento.</p>
					<p>Portanto, o <strong>CLCB</strong> é um documento essencial para a regularização do imóvel, pois é uma autorização para o funcionamento do local. Esta liberação possui algumas informações do imóvel como endereço, metragem, informações cadastrais, entre outras. Alguns certificados possuem data de validade, como prazo de um, dois ou cinco anos.</p>
					<p>O proprietário do imóvel é responsável em manter o documento sempre atualizado, ou seja, se o município em questão tiver uma data de vencimento, é importante que dois meses antes do vencimento, o procedimento seja  repetido para renovação a documentação. É necessário que seja exposto em local visível e seguro principalmente quando se tratar de comércio, pois quando for vistoriado a documentação estará facilmente visível.</p>
					<h2>QUAIS AS CONSEQUÊNCIAS PARA QUEM NÃO TEM O CLCB?</h2>
					<p>O proprietário pode sofrer algumas consequências caso o imóvel não tenha o certificado emitido, principalmente quando o local possui grande circulação de pessoas, como é o caso  dos comércios.</p>
					<p>Os bombeiros têm exigido cada vez mais que as normas sejam seguidas e que os locais sejam aptos a receber pessoas e oferecer mais segurança. Para garantir isso, as vistorias tem acontecido com maior frequência nas edificações. Caso no ato da vistoria o imóvel não atender às exigências da normativa, o proprietário poderá ser inicialmente notificado ou multado,  em casos mais graves, o local poderá ser interditado.</p>
					<p>O imóvel permanece sob responsabilidade do proprietário mesmo após a emissão do <strong>CLCB</strong> e do PPCI, pois mesmo que esses documentos sejam provados, os equipamento instalado devem ser frequentemente analisados por um responsável técnico, a fim de garantir que estão em perfeito funcionamento e que não apresentam falhas na operação.</p>
					<h2>O IMÓVEL É REGULARIZADO COM O PROJETO?</h2>
					<p>Muitas pessoas pensam que apenas a aprovação do projeto é suficiente para regularizar o  imóvel e quando são vistoriadas alegam que já possuem  o projeto aprovado, tudo isso por falta de informação e conhecimento em como proceder.</p>
					<p>Porém para que o local tenha autorização do Corpo o bombeiro é necesssário ccumprir todas as etps do processo, ou seja, é necessário aguardar pela emissão do <strong>CLCB</strong> e mantê-lo atualizado sem que se aproxime da data de vencimento, Por isso, é recomendado que a renovação seja iniciada dois meses antes do documento vencer.</p>
					<h2>COMO PROCEDER APÓS A EMISSÃO DO CERTIFICADO?</h2>
					<p>Assim que houver a emissão do <strong>CLCB</strong>, é necessário estar atento a data de validade do certificado, pois é necessário que o requerente ou proprietário compareça no Corpo e bombeiro com um período de antecedência para a renovação o documento, no caso dos municípios em que possuem vencimento.</p>
					<p>Será necessário solicitar uma nova vistoria a fim dos bombeiros verificarem se o local atende os princípios básico de segurança e se os equipamentos anteriormente aprovados continuam em funcionamento, se sim, o certificado será renovado, no caso do sistema contra incêndio apresentar falhas o bombeio emitir um relatório com as adequações e esse processo se repete até que o <strong>CLCB</strong> seja emitido.</p>
					<p>No caso da renovação da licença, uma vez em que o projeto for aprovado, não será necessário renová-lo novamente, sendo necessário apenas a atualização do certificado, o projeto só deve ser alterado quando houver alterações na estrutura que exija grande intervenção.</p>
					<h2>O QUE É FAT?</h2>
					<p>Nesse caso acontece quando o projeto de incêndio é aprovado e a <strong>CLCB</strong> é emitida, porém após um tempo, o proprietário busca realizar pequenas intervenções no imóvel que tem interferência na estrutura, como é o caso de descrimo ou acréscimo de metragem sendo necessário a atualização do projeto inicial.</p>
					<p>Esse processo de atualização é considerada um FAT, ou  seja, quando há  atualização do PPCI mediante a pequenas intervenções no imóvel, ms para os casos de grandes intervenções que alterem grande parte do imóvel e sua estrutura é necessário aprovar um novo projeto.</p>
					<h2>DE QUEM É A REPONSABILIDADE DO FUNCIONAMENTO DOS EQUIPAMENTOS?</h2>
					<p>Como vimos, mesmo que o Corpo de Bombeiros tenha aprovado e emitido o <strong>CLCB</strong>, é necessário que o proprietário junto de um responsável técnico mantenha a integridade dos equipamentos  fim de garantir que tenham um bom funcionamento.</p>
					<p>Por isso, é imprescindível que os itens de incêndio passem por manutenção frequentemente, pois precisam ser analisados e testados, devido serem considerados itens de segurança é fundamental que estejam em operação adequada caso surja situação de emergência  fim de conseguir proporcionar segurança, especialmente quando se trata de incêndios. Veja abaixo alguns os principais itens que são considerados no momento da manutenção:</p>
					<ul class="topicos-relacionados">
						<li>Extintores;</li>
						<li>Central de alarme;</li>
						<li>Hidrantes;</li>
						<li>Sprinkler;</li>
						<li>Detectores de fumaça.</li>
					</ul>
					<p>Existem muito fabricante no mercado que oferecem serviços de manutenção tamb´m, o que facilita, pois estes equipamentos podem ter algumas particularidades que variam  de fabricação, por isso, é interessante que a manutenção seja feita pela própria empresa que fabricou, nos casos em que existe essa possibilidade.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>