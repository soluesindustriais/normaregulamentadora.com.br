<? $h1 = "Auto de licença de funcionamento";
$title  = "Auto de licença de funcionamento";
$desc = "Encontre $h1, você só encontra no portal Soluções Industriais, receba os valores médios online com mais de 200 fornecedores ao mesmo tempo";
$key  = "Auto de licença em funcionamento,Fazer auto de licença de funcionamento";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section>
				<?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/auto-de-licenca-de-funcionamento-01.jpg" title="<?=$h1?>" class="lightbox"><img class="lazyload" data-src="<?=$url?>imagens/mpi/thumbs/auto-de-licenca-de-funcionamento-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/auto-de-licenca-de-funcionamento-02.jpg" title="Auto de licença em funcionamento" class="lightbox"><img class="lazyload" data-src="<?=$url?>imagens/mpi/thumbs/auto-de-licenca-de-funcionamento-02.jpg" title="Auto de licença em funcionamento" alt="Auto de licença em funcionamento"></a><a href="<?=$url?>imagens/mpi/auto-de-licenca-de-funcionamento-03.jpg" title="Fazer auto de licença de funcionamento" class="lightbox"><img class="lazyload" data-src="<?=$url?>imagens/mpi/thumbs/auto-de-licenca-de-funcionamento-03.jpg" title="Fazer auto de licença de funcionamento" alt="Fazer auto de licença de funcionamento"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
					<h2>ENTENDA O QUE É O AUTO DE LICENÇA DE FUNCIONAMENTO E QUAL A SUA IMPORTÂNCIA</h2>
					<p>Toda empresa ou estabelecimento, antes de abrir ou mesmo que já esteja iniciada, necessita de um alvará de funcionamento, também denominado <strong>Auto de licença de funcionamento</strong>. Esse documento tem a função de legalizar e permitir que o empreendimento possa operar de forma condizente às legislações.</p>
					<p>]Trata-se de uma autorização que permite que o negócio possa exercer suas atividades, seja ele um estabelecimento comercial, uma indústria ou prestadores de serviço: todos precisam obtê-lo para que possam atuar e desenvolver suas atividades. Mesmo que o empreendimento se trate de um negócio online, caso exista uma sede física para estocar os produtos, ela precisará do <strong>Auto de licença de funcionamento</strong> para estar devidamente legalizado.</p>
					<p>É importante atentar-se ao documento antes mesmo de iniciar o negócio, pois pode acontecer de você alugar ou comprar um imóvel e, dependendo da atividade a ser realizada, não ser permitido atuar no local escolhido. Um exemplo a ser dado se refere à abertura de uma empresa no local onde se mora, ou seja, na residência. É permitido, desde que as funções ali exercidas não envolvam armazenamento, carga e descarga de mercadorias. Além disso, não é permitido circulação de muitas pessoas no ambiente.</p>

					<p>Veja também <a href="https://www.normaregulamentadora.com.br/emissao-de-avcb-sp" style="cursor: pointer; color: #006fe6;font-weight:bold;">Emissão de AVCB SP</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>

					<h2>QUEM FORNECE?</h2>
					<p>Geralmente, a Prefeitura é a responsável por conceder o documento. No entanto, existem outros órgãos municipais que também podem emiti-lo, alegando o direito de exercer as atividades no ponto do estabelecimento conforme as normas existentes.</p>
					<h2>QUEM PRECISA?</h2>
					<p>Qualquer tipo de negócio que possua circulação de público, independente da área de atuação, precisa ter o <strong>Auto de licença de funcionamento</strong> devidamente emitido pelo poder público de sua cidade.</p>
					<p>Fábricas, estádios, restaurantes, lanchonetes, salões de beleza, cinemas, bares, oficinas mecânicas, farmácias... todos esses e muitos outros estabelecimentos constam na lista de obrigatoriedade da obtenção do documento. Num geral, os seguintes estabelecimentos devem possuir, obrigatoriamente, o documento:</p>
					<ul class="topicos-relacionados">
						<li>Comércios;</li>
						<li>Prestadores de serviço;</li>
						<li>Indústrias;</li>
						<li>Empresas.</li>
					</ul>
					<p>Apesar de praticamente todo negócio precisar da liberação que esse alvará fornece, existem variações que especificam cada um dos estabelecimentos de acordo com as atividades que ali são desenvolvidas.</p>
					<h2>VARIAÇÕES DO AUTO DE LICENÇA DE FUNCIONAMENTO</h2>
					<p>Existem quatro tipos de variações relativas ao documento que legaliza e permite que as atividades do estabelecimento possam ser desenvolvidas de forma condizente às normas existentes. As variações existentes são:</p>
					<p><strong>Auto de Licença de Funcionamento</strong>, também conhecido pela sigla ALF e válido para os imóveis não residenciais que possuem atividades comerciais, industriais ou prestadoras de serviços em seu funcionamento.</p>
					<p><strong>Auto de Licença de Funcionamento</strong> Condicionado, ou ALF-C, é o documento direcionado para as edificações que ainda estão irregulares, mas que estão em busca da regularização. Com este documento, elas podem continuar realizando suas atividades enquanto procurar regularizar-se.</p>
					<p>Alvará de funcionamento de Local de Reunião, também conhecido pela sigla ALF, é o documento solicitado para todos os lugares que possuem reuniões de indivíduos. Bares, lanchonetes, cinemas e outros estabelecimentos que possuam capacidade igual ou maior do que 250 pessoas devem possuir esse tipo de variação de alvará.</p>
					<p>Alvará de autorização para eventos públicos e temporários: necessário para os lugares que serão realizados eventos públicos, mesmo que temporários, para um público de mais de 250 pessoas, independente de se tratar de um imóvel privado ou de um imóvel público.</p>
					<h2>PASSO A PASSO PARA A OBTENÇÃO DO ALVARÁ</h2>
					<p>Primeiramente, deve-se realizar uma análise para descobrir se a atividade que se deseja exercer é permitida na localização que o imóvel possui. Caso seja alugado, é importante verificar se a planta já possui o <strong>Auto de licença de funcionamento</strong> e quais foram as circunstâncias que ele foi emitido, pois podem existir mudanças a serem feitas ou renovadas, dependendo do empreendimento que será realizado.</p>
					<p>Se você possui um sócio no negócio, certifique-se que ele não entrou com a solicitação, pois só é permitido realizar o registro de um CNPJ no endereço. Existem algumas cidades que permitem que o pedido do alvará seja realizado pela internet, e em outras cidades que exigem que o proprietário compareça até a Prefeitura ou à divisa específica que cuida de documentos deste tipo.</p>
					<p>O processo de vistorias e algumas outras etapas como a necessidade de determinados documentos, assim como o pedido, também são fatores que variam conforme o município em que os empreendimentos estão alocados, pois cada um possui suas especificações.</p>
					<p>Deve-se reunir todos os documentos necessários e, se for o caso, levá-los até a Prefeitura ou órgão responsável para solicitar o pedido. Ter em mãos o alvará emitido pelo Corpo de Bombeiros, na maioria das cidades, é de extrema importância, pois isso garante a segurança do local para aquelas pessoas que ali transitarão.</p>
					<p>Após reunir os documentos e certificações necessárias, o empreendedor analisa em qual situação a sua empresa se encaixa, pois isso definirá o tipo de alvará que será emitido. Para essa análise e também para ajudar na reunião dos documentos, pode-se contratar uma empresa ou profissional especializado e qualificado, a fim de reduzir os trabalhos.</p>
					<p>Existem diversas empresas e profissionais especializados na área de documentos burocráticos para empreendedores e seus empreendimentos. Como são especialistas no ramo, podem prestar atendimentos e consultorias com eficiência e colaborar para que o alvará consiga ser emitido com mais agilidade e facilidade. Indica-se, sempre, pesquisar profissionais de renome que possuam boas indicações.</p>
					<h2>QUAIS OS DOCUMENTOS PARA CONSEGUIR O AUTO DE LICENÇA DE FUNCIONAMENTO?</h2>
					<p>Para que seja possível obter o <strong>Auto de licença de funcionamento</strong> para o seu empreendimento, existem determinados documentos que precisam ser apresentados. São eles:</p>
					<ul class="topicos-relacionados">
						<li>CPF e RG do responsável pelo empreendimento;</li>
						<li>Planta do imóvel onde o negócio será aberto;</li>
						<li>Cópia do recibo do IPTU em dia;</li>
						<li>Setor, quadra e lote (SQL) do imóvel;</li>
						<li>Cadastro do Contribuinte Mobiliário;</li>
						<li>Certificado de conclusão de imóvel recém-construído;</li>
						<li>Declaração de atividade do imóvel.</li>
					</ul>
					<h2>LICENÇAS NECESSÁRIAS</h2>
					<p>Para que o <strong>Auto de licença de funcionamento</strong> seja validado, são necessárias determinadas licenças que envolvem órgãos variados como a Prefeitura municipal, Vigilância Sanitária e o Corpo de Bombeiros, por exemplo. Essas licenças variam de acordo com o nível de complexidade que o empreendimento apresenta e também de acordo com a localidade do empreendimento. São elas:</p>
					<p>Auto de vistoria do Corpo de Bombeiros: também conhecido pela sigla AVCB, assim como o próprio nome já indica, é realizada pelo Corpo de Bombeiros, órgão responsável por verificar as normas de seguranças existentes e as que devem ser desenvolvidas no local do empreendimento. Todas os negócios precisam obter esse tipo de licença.</p>
					<p>Licença Sanitária: este tipo de documento envolve funções relativas aos órgãos municipais, estaduais e também federais de vigilância sanitária (ANVISA). Geralmente, costuma ser necessário para os empreendimentos que possuem atuação no ramo de cosméticos, alimentos, higiene e perfumarias, medicamentos e outros tipos de produtos farmacêuticos e para a saúde.</p>
					<p>Licença ambiental: esta licença deve ser obtida pelas empresas do ramo da metalurgia, mecânica, transportes, têxteis, de calçados, químicas, turismo, atividades agropecuárias, entre outras. O órgão responsável por expedir este documento são os municipais e estaduais referentes ao meio ambiente e o Ibama.</p>
					<p>Produtos de origem animal: essa licença se faz necessária entre as empresas que produzem produtos de origem animal, especialmente para consumo humano que possuem comércio internacional ou mesmo entre estados diferentes. O órgão expedidor que provê o documento é o Ministério da Agricultura, Pecuária e Abastecimento.</p>
					<h2>QUAIS OS RISCOS DE NÃO POSSUIR O AUTO DE LICENÇA DE FUNCIONAMENTO?</h2>
					<p>Caso o documento seja inexistente, diversos transtornos podem ocorrer. Caso seja sinalizado, em uma vistoria, que o estabelecimento não possui o <strong>Auto de licença de funcionamento</strong>, o proprietário do empreendimento é sinalizado e possui um período de até trinta dias para conseguir providenciá-lo. Caso não consiga obtê-lo dentro desse período, o dono do negócio precisa arcar com um valor aproximado de dois mil reais.</p>
					<p>Dependendo da circunstância o estabelecimento pode, até mesmo, ser fechado de forma definitiva, fazendo com que o registro profissional seja cancelado. Além disso, dependendo do caso e da atividade ali exercida, também podem ocorrer apreensão de bens e mercadorias.</p>
					<h2>QUAIS OS PROCEDIMENTOS PARA SOLICITAR O ALVARÁ?</h2>
					<p>Para que a Prefeitura Municipal ou outro órgão governamental municipal possa emitir o <strong>Auto de licença de funcionamento</strong>, é necessário que o responsável pela empresa reúna todas as condições e documentos que são exigidos pela legislação.</p>
					<p>A forma de pedido do alvará é variável de município para município, com determinadas condições específicas. Existem cidades que o pedido pode ser realizado pela internet, e outras que exigem o comparecimento do responsável pelo empreendimento até a prefeitura ou em uma divisão específica da mesma.</p>
					<p>Da mesma forma que outros processos burocráticos relativos à empresas, existem taxas que devem ser pagas para que o alvará de funcionamento seja emitido. O valor varia de acordo com a localização do empreendimento, pois a Prefeitura que regulamenta essa taxa.</p>
					<p>As informações a serem reunidas variam, também, conforme o tipo de atividade exercida no local em que está sendo necessário o <strong>Auto de licença de funcionamento</strong>, pois as funções que ali são realizadas irão fazer com que o tipo de licença seja variável. Em relação às atividades se faz necessário, também, verificar se elas são permitidas de serem realizadas no local escolhido para o empreendimento.</p>
					<p>Caso o imóvel do negócio seja alugado e não comprado, pode ser que ele já possua um <strong>Auto de licença de funcionamento</strong>. Deve-se atentar, no entanto, em relação às alterações que serão realizadas no local, para, assim, renovar o documento. É necessário que o <strong>Auto de licença de funcionamento</strong> esteja relacionado a sua própria empresa e as atividades que desenvolverá, não em relação ao empreendimentos e atividades que funcionava ali anteriormente.</p>
					<h2>O AUTO DE LICENÇA DE FUNCIONAMENTO TEM VALIDADE?</h2>
					<p>A validade do <strong>Auto de licença de funcionamento</strong> é por tempo indeterminável. No entanto, caso ocorra algum tipo de alteração nas informações que foram apresentadas para conseguir a obtenção do documento, é necessário renová-lo.</p>
					<p>Essas alterações podem ser relativas ao tipo ou as características das atividades e funções ali realizadas, à razão social ou da propriedade do empreendimento, aumento ou diminuição da área da edificação que o estabelecimento funciona, por passagem do prazo de validade de dois anos, no caso de do <strong>Auto de licença de funcionamento</strong> condicionado, entre outros fatores.</p>
					<p>Caso ocorra uma reforma que ocasionou na adição de um cômodo no imóvel, por exemplo, é necessário realizar a renovação do alvará. Se perder o documento original, é necessário solicitar a segunda via do mesmo. Geralmente, essa solicitação pode ser feita online ou, dependendo do município, presencialmente na Prefeitura ou órgão responsável.</p>
					<h2>O ALVARÁ PODE EXIGIR MUDANÇAS?</h2>
					<p>Sim, o alvará pode realizar exigências relacionadas à mudanças na planta ou até mesmo no prédio já construído, como adicionar mais recursos de acessibilidade para pessoas com deficiência, e modificar ou adicionar banheiros, por exemplo.</p>
					<p>É comum que realizem vistorias para a verificação das mudanças que foram apontadas, a fim de analisar se tudo está devidamente funcionando conforme o que foi combinado. Caso a situação não esteja regularizada, existem chances de se perder o alvará e, com isso, o empreendimento está, automaticamente, irregular.</p>
					<h2>DOCUMENTO IMPORTANTE</h2>
					<p>O <strong>Auto de licença de funcionamento</strong> é de extrema importância e indispensável na maioria dos negócios pois, sem o documento, não é possível abrir as portas da empresa. A responsabilidade em estabelecer os critérios para que o alvará seja garantido fica sob função das Prefeituras de cada município, pois cada um deles possui suas especificidades em relação aos empreendimentos que ali são desenvolvidos.</p>
					<h2>QUANTO DEMORA DEMORA PARA FICAR PRONTO?</h2>
					<p>O tempo para obter o documento pode variar, dependendo do caso e do órgão responsável. Assim que a documentação necessária é levada até a Prefeitura, inicia-se um processo interno para verificar que o imóvel está devidamente adequado para o empreendimento que deseja iniciar. Caso o local não possua uma cozinha, por exemplo, fica inviável abrir um restaurante. Ou, se deseja abrir um hotel, é necessário que a construção possua quartos, obviamente.</p>
					<p>Um fiscal responsável por visitar e avaliar o estabelecimento pode ocasionar em demoras na obtenção, pois sua vistoria pode apontar modificações necessárias para que a estrutura fique devidamente apta para as atividades que serão realizadas. Apesar de variar de acordo com o município, é comum que a obtenção da documentação não seja demorado após o empreendedor dar entrada no processo.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>