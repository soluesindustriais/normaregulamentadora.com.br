<? $h1 = "Auto de vistoria do corpo de bombeiros";
$title  = "Auto de vistoria do corpo de bombeiros";
$desc = "Orce $h1, veja os melhores fabricantes, faça um orçamento hoje com aproximadamente 500 distribuidores ao mesmo tempo";
$key  = "Auto de vistoria dos bombeiros,Fazer auto de vistoria do corpo de bombeiros";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?><div class="wrapper">
	<main>
		<div class="content">
			<section><?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/auto-de-vistoria-do-corpo-de-bombeiros-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/auto-de-vistoria-do-corpo-de-bombeiros-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/auto-de-vistoria-do-corpo-de-bombeiros-02.jpg" title="Auto de vistoria dos bombeiros" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/auto-de-vistoria-do-corpo-de-bombeiros-02.jpg" title="Auto de vistoria dos bombeiros" alt="Auto de vistoria dos bombeiros"></a><a href="<?=$url?>imagens/mpi/auto-de-vistoria-do-corpo-de-bombeiros-03.jpg" title="Fazer auto de vistoria do corpo de bombeiros" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/auto-de-vistoria-do-corpo-de-bombeiros-03.jpg" title="Fazer auto de vistoria do corpo de bombeiros" alt="Fazer auto de vistoria do corpo de bombeiros"></a>
					</div>
					<span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
					<h2>AUTO DE VISTORIA DO CORPO DE BOMBEIROS: COMO OBTER?</h2>
					<p>As normas de combate a incêndio são válidas aos imóveis residenciais e comerciais, porém para cada tipo de imóvel existem particularidades especificadas na própria norma. A única exceção para algumas exigências de combate ao incêndio, são para residências  unifamiliares, consideradas quando habitadas por uma única família, aos demais imóveis a norma deve ser seguida a risca.</p>
					<p>Os bombeiros agem além de combates ao incêndio e outros acidentes, são responsáveis também pela fiscalização em edificações a fim de identificar a regularidade do imóvel em relação às normas pois especificam os itens de incêndio necessário para cada tipo de imóvel e quando regulares e aprovados conseguem a certificação denominada por <strong>Auto de vistoria do corpo de bombeiros</strong>, conhecida pela sigla AVCB.</p>
					<p>Mas existem algumas etapas a serem seguidas a fim da obtenção da certificação e a primeira é a elaboração do projeto de prevenção de combate ao incêndio, o qual chamamos de PPCI.</p>
					<h2>QUAIS SÃO OS CRITÉRIOS PARA ELABORAR O PROJETO DE INCÊNDIO?</h2>
					<p>O projeto de incêndio depende da elaboração da planta arquitetônica, pois nessa primeira planta estará o esqueleto do imóvel considerando a metragem, áreas, janelas, portas e outros elementos componentes do imóvel e a partir desse esqueleto definido será possível analisar as normas de combate ao incêndio e identificar quais são as exigências para o tipo de imóvel mencionado na planta arquitetônica.</p>
					<p>Após esse levantamento, o profissional responsável pela elaboração do projeto de incêndio irá criá-lo considerando todos os equipamentos e quantidades necessários para cada área da edificação.</p>
					<p>É importante o profissional estar atento às particularidades da norma e das exigências municipais e podem interferir em alguns aspectos para determinada região, metragem e função do imóvel.</p>
					<p>Outro aspeto essencial é o profissional responsável pelo projeto estar cadastrado no CREA (Conselho Federal de Engenharia e Agronomia), pois esse cadastro é mais uma garantia da aptidão do profissional, sendo na maioria da vezes engenheiros, mas para o caso de arquitetos, existe outro sistema conhecido como CAU (Conselho de Arquitetura e Urbanismo).</p>
					<p>É importante mencionar pois o estado de São Paulo o Auto de vistoria do corpo e bombeiro é exigido apenas aos estabelecimento com área superior a 750 metros e com volume maior de 250 pessoas em circulação.</p>
					<h2>COMO FUNCIONA O PROCESSO DE APROVAÇÃO DO PROJETO?</h2>
					<p>Após a elaboração do projeto é necessário outros documentos estejam anexados como o memorial descritivo e ART (Anotação de responsabilidade técnica), pois assim as plantas estiverem acompanhadas desses anexos, o requerente deve comparecer ao Corpo e Bombeiro municipal para apresentação dessas documentações para protocolo. Veja abaixo quais documentos são necessários a fim do processo ser analisado e iniciado:</p>
					<ul class="topicos-relacionados">
						<li><strong>Planta de Incêndio:</strong> É composta pelo esqueleto do imóvel acompanhado de símbolos representantes dos equipamentos de incêndio a fim de serem identificados através de uma legenda.</li>
						<li><strong>Memorial descritivo:</strong> É um complemento da planta e descreve todos os cálculos e específica de forma detalhada todos os equipamentos representados na planta.;</li>
						<li><strong>A anotação de responsabilidade técnica:</strong> este documento quando assinado por um responsável técnico, o responsabiliza pelas questões técnicas de um projeto ou obra;</li>
						<li><strong>Taxa de análise de projeto (DAE):</strong> Trata-se de uma taxa estadual para o processo de análise do projeto seja iniciado.</li>
					</ul>
					<p>Após todos esses documentos estiverem disponíveis o requerente deve comparecer ao Corpo de Bombeiros, onde será feito o protocolo dessas documentações e em seguida o requerente deverá aguardar o período de análise. Em São Paulo, a média de análise são de 30 dias, porém esse período varia para cada município, por isso o acompanhamento deve ser frequente sendo presencial ou pela internet.</p>
					<p>Ao final da análise do o projeto, o sistema é atualizado, na maioria dos casos o acompanhamento é feito pela internet a fim de facilitar o andamento do processo. Assim a planta é analisada por um bombeiro é possível ter duas  possibilidades sendo o projeto aprovado ou reprovado.</p>
					<p>Se aprovado o responsável pela obra tem a liberação a fim de executar e os serviços são liberados, porém é fundamental a obra ser executada exatamente como consta no projeto. Quando reprovado o bombeiro emite um comunicado com todas as regularizações necessárias para serem ajustadas em projeto e junto o comunicado é imposto um prazo para cumprimento.</p>
					<p>Assim ao final dos ajustes, o requerente deve comparecer novamente ao Corpo de Bombeiros para um novo protocolo e uma nova análise e esse procedimento é repetido até  o projeto ser aprovado e obra seja liberada para execução.</p>
					<h2>COMO OBTER O AUTO DE VISTORIA DO CORPO DE BOMBEIROS?</h2>
					<p>Após a aprovação do projeto, a obra é liberada para execução e nesse momento é de extrema importância a fim de todos os pontos serem mencionados em projeto sejam considerados no momento da  execução, pois após a finalização a obra o requerente deverá solicitar vistoria aos bombeiros.</p>
					<p>No momento da vistoria, o bombeiro  vai analisar se todos os ponto do projeto aprovado foram considerados e se os equipamentos instalados estão funcionando corretamente. Se o imóvel for aprovado o certificado é emitido apó dias e o edifício passa a ser regular com o órgão.</p>
					<p>Quando reprovado, é emitido um comunicado com todas as adequações a fim de serem regularizadas e um prazo é estipulado para a finalização essas adequações, sendo importante o proprietário providenciar a regularização imediata. Existem alguns casos específicos de obras complexas e nesse caso o requerente deve solicitar um prazo maior quando considerar inviável cumprir dentro do prazo inicial.</p>
					<p>No caso de reprovação, quando as adequações estiverem finalizadas, o requerente deve solicitar nova vistoria. Esse procedimento se repete até que a obra seja finalizada e o certificado seja emitido.</p>
					<p>É importante mencionar a aprovação do projeto não licencia o imóvel, sendo necessário aguardar a emissão do <strong>Auto de vistoria do corpo de bombeiros</strong> para considerar a edificação    regular com o órgão.</p>
					<h2>PORQUE O AUTO DE VISTORIA DO CORPO DE BOMBEIROS É IMPORTANTE ?</h2>
					<p>O AVCB (Auto de Vistoria do Corpo de Bombeiro) é o documento responsável pela regularização da edificação com as normas de incêndio, quando esse documento é emitido significa cumprimento de todas as etapas, como a aprovação do projeto e a aprovação da obra, atestando o perfeito funcionamento dos equipamentos.</p>
					<p>É importante mencionar, pois após a emissão do  <strong>Auto de vistoria do corpo de bombeiros</strong> seja emitido o proprietário continua sendo responsável pelo imóvel e o documento não ausenta a responsabilidade do responsável técnico e proprietário caso sinistros o ocorram no local.</p>
					<p>O certificado é constituído por diversas informações sobre o imóvel, como endereço, metragem, informações cadastrais, entre outras informações.</p>
					<p>Dependendo do município o certificado possui data de validade, esse prazo varia de um, dois ou cinco anos.</p>
					<p>É responsabilidade do proprietário manter o Auto de vistoria do corpo de bombeiro vigente e atualizado, por isso é importante estar atento a data de validade quando houver. Outra exigência feita refere-se a exposição do <strong>auto de vistoria do corpo de bombeiros</strong>, pois é fundamental que estejam expostas em local visível para facilitar quando ocorrer vistorias no local.</p>
					<h2>QUAIS OS RISCOS PARA QUEM NÃO TEM O AVCB EMITIDO?</h2>
					<p>Os bombeiros têm sido cada vez mais exigentes, ainda mais com edifícios comerciais pois possuem um grande movimentação de pessoas, por isso, as vistorias têm sido cada vez  mais frequentes.</p>
					<p>Essas vistorias tem como objetivo conferir se os estabelecimentos têm o <strong>Auto de vistoria do corpo de bombeiros</strong> emitido e se os equipamentos estão em perfeito funcionamento, caso esses pontos não estiverem regular o proprietário poderá ser multado ou o estabelecimento poderá ser interditado.</p>
					<p>Mesmo sendo o <strong>Auto de vistoria do corpo de bombeiros</strong>  emitido e o PPCI seja aprovado, o riscos permanecem e o imóvel continua sujeito a sofrer situações de risco, porém esses procedimentos e equipamentos de incêndio deixam o imóvel apto ao combate o fogo e ajuda a prevenir danos maiores.</p>
					<h2>O PROJETO DE INCÊNDIO LICENCIA O IMÓVEL?</h2>
					<p>Além da aprovação do projeto é necessário o Auto de vistoria o corpo de bombeiros seja emitido a fim do imóvel seja considerado regular. Muitas pessoas consideram apenas a planta carimbada pelo corpo de bombeiro basta, porém é necessário o certificado seja emitido.</p>
					<h2>O QUE FAZER APÓS A APROVAÇÃO ?</h2>
					<p>Após a emissão do <strong>Auto de vistoria do corpo de bombeiros</strong>, o proprietário precisa estar atento a data de validade para providenciar a renovação, por isso, é aconselhável que esse procedimento seja feito dois meses antes do vencimento.</p>
					<p>Assim que o documento estiver prestes a vencer o requerente ou proprietário deve solicitar uma nova vistoria, em alguns casos esses processo é feito online ou presencialmente, dependerá do atendimento do órgão municipal.</p>
					<p>É possível que mesmo que o <strong>Auto de vistoria do corpo de bombeiros</strong> seja emitido anteriormente, no ato da vistoria para renovação da licença o imóvel seja reprovado e isso ocorre devido a falhas no s equipamento ou algumas alterações na estrutura do imóvel.</p>
					<h2>O QUE SIGNIFICA FAT?</h2>
					<p>Ocorre quando um projeto é aprovado e executado e após um tempo a obra sofre intervenções estruturais com acréscimo ou decréscimo de área, sendo necessário atualizar o projeto inicial.</p>
					<p>Quando as alterações são simples como mudanças de layouts é possível fazer uma FAT que significa pequenas alterações para um projeto já existente, mas quando as intervenções são grandes é necessário que um novo projeto seja iniciado e aprovado novamente, ainda mais quando a área do imóvel é alterada.</p>
					<h2>QUEM É RESPONSÁVEL EM MANTER OS EQUIPAMENTOS EM FUNCIONAMENTO?</h2>
					<p>Mesmo que o Auto de vistoria do corpo de bombeiro seja emitido, o proprietário é responsável por manter os equipamentos em perfeito funcionamento. Para isso existem empresas que prestam serviço de manutenção mensal para garantir que os equipamento não  apresentem falhas principalmente no ato da vistoria, mas essa preocupação precisa ser diária, é essencial que os equipamentos sejam acompanhados por responsáveis técnicos.</p>
					<p>Muitos fabricantes oferecem serviço completo, como a instalação e manutenção e isso facilita pois cada equipamento possui  particularidades. O serviço preventivo é fundamental quando o assunto é segurança, pois através dessas manutenções frequentes é possível evitar alguns riscos e a falha dos equipamentos em um situação de emergência.</p>
					<p>Mediante a variedade e equipamento e complexidade é fundamental que seja acompanhada por um profissional técnico e de confiança. Confira alguns equipamentos de incêndio que são considerados os principais para a emissão do <strong>Auto de vistoria do corpo de bombeiros</strong>:</p>
					<h2>1- EXTINTORES</h2>
					<p>Os extintores são itens fundamentais para o combate ao incêndio, pois ajudam a controlar o fogo e para cada situação de emergência existe um tipo de extintor, sendo classificados como classe A (combate o fogo em materiais como madeira e papel), classe B (combate o fogo causado por gases inflamáveis) e classe C (combate o fogo ocasionado em equipamentos elétricos).</p>
					<h2>2- SPRINKLER</h2>
					<p>É um modelo e chuveiro automático e quando exposto a altas temperaturas o seu vidro rompe e libera uma descarga de água para combater o fogo, mas é importante que esse equipamento não seja obstruído ou instalado em locais com sistemas elétricos, sendo geralmente instalado em  estoques devido ao alto nível de caixas e papéis.</p>
					<h2>3- CENTRAL DE ALARME DE INCÊNDIO</h2>
					<p>São sensores que captam quando h´fumaça em determinado ambiente e quando isso ocorre é liberado um sinal para o painel de controle que com a sirene que emite um som alto para que as pessoas abandonem o local.</p>
					<h2>4- HIDRANTES</h2>
					<p>É uma tubulação que permite maior saída de água com maior pressão e vem acompanhada de uma mangueira resistente e extensa que consegue alcance maior das área atingidas por um incêndio.</p>
					<p>Além desses existem muitos outros equipamentos e soluções para ajudar em uma situação de risco como as placas de sinalização e saídas de emergência. É importante que sempre esteja atento quando estiver em um local e assim que chegar tente identificar a saída de emergência em casos de risco.</p>
					<p>Para os proprietários de estabelecimento comerciais é preciso uma atenção extra para manter o funcionamento os equipamento de incêndio, pois  fiscalização é ainda maior para esses locais.</p>
					<p>O certificado emitido pelos bombeiros é fundamental para o funcionamento e qualquer edifício, seja residencial ou comercial e ajuda a prevenir e adaptar o ambiente para encarar de emergência com maior segurança.</p>
				</article>
				<? include('inc/coluna-mpi.php');?>
				<br class="clear">
				<? include('inc/busca-mpi.php');?>
				<? include('inc/form-mpi.php');?>
				<? include('inc/regioes.php');?>
			</section>
		</div>
	</main>
</div>
<? include('inc/footer.php');?>
</body>
</html>