<? $h1 = "Laudo corpo de bombeiros"; $title  = "Laudo corpo de bombeiros"; $desc = "Receba os valores médios de $h1, encontre as melhores empresas, receba diversos orçamentos agora mesmo com dezenas de distribuidores ao mesmo tempo"; $key  = "Laudos corpo de bombeiros,Emissão de laudo corpo de bombeiros"; include('inc/head.php'); include('inc/fancy.php'); ?>
</head>

<body>
    <? include('inc/topo.php');?>
    <div class="wrapper">
        <main>
            <div class="content">
                <section><?=$caminhoinformacoes?><br class="clear" />
                    <h1><?=$h1?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?=$url?>imagens/mpi/laudo-corpo-de-bombeiros-01.jpg"
                                title="<?=$h1?>" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/laudo-corpo-de-bombeiros-01.jpg" title="<?=$h1?>"
                                    alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/laudo-corpo-de-bombeiros-02.jpg"
                                title="Laudos corpo de bombeiros" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/laudo-corpo-de-bombeiros-02.jpg"
                                    title="Laudos corpo de bombeiros" alt="Laudos corpo de bombeiros"></a><a
                                href="<?=$url?>imagens/mpi/laudo-corpo-de-bombeiros-03.jpg"
                                title="Emissão de laudo corpo de bombeiros" class="lightbox"><img
                                    src="<?=$url?>imagens/mpi/thumbs/laudo-corpo-de-bombeiros-03.jpg"
                                    title="Emissão de laudo corpo de bombeiros"
                                    alt="Emissão de laudo corpo de bombeiros"></a></div><span class="aviso">Estas
                            imagens foram obtidas de bancos de imagens públicas e disponível livremente na
                            internet</span>
                        <hr />
                        <p>O <strong>laudo Corpo de Bombeiros</strong> ou <a href="https://www.normaregulamentadora.com.br/laudo-avcb" class="linkText"> laudo AVCB </a> é um documento que toda empresa,
                            indústria ou instituições públicas devem ter com o intuito de garantir o preenchimento dos
                            requisitos de segurança contra incêndio e o seguimento de normas de segurança previstas em
                            lei. Esse documento deve ser solicitado e emitido pelo órgão competente de cada Estado.</p>
                        <h2>Obrigatoriedades exigidas</h2>
                        <p>Os profissionais responsáveis só vão emitir o laudo para quem estiver com os diversos itens
                            obrigatórios de acordo com as normas regidas por órgãos competentes. Alguns desses itens
                            serão listados a seguir.</p>
                        <ul>
                            <li class="li-mpi">Brigada de incêndio;</li>
                            <li class="li-mpi">Para-raios;</li>
                            <li class="li-mpi">Laudo elétrico;</li>
                            <li class="li-mpi">Laudo de sistema de combate ao incêndio;</li>
                            <li class="li-mpi">CMAR.</li>
                        </ul>
                        <h2>Outras informações</h2>
                        <p>Para que uma empresa esteja apta para conseguir o <strong>laudo Corpo de Bombeiros</strong>,
                            é importante que, mesmo antes da solicitação, realize consultoria com uma empresa
                            especializada em sistemas de incêndio.</p>
                        <p>Faça um orçamento pelo formulário abaixo. É simples, rápido e gratuito!</p>

                        <p>Veja também <a href="https://www.normaregulamentadora.com.br/auto-de-vistoria-do-corpo-de-bombeiros" style="cursor: pointer; color: #006fe6;font-weight:bold;">Auto de Vistoria do Corpo de Bombeiros</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>

                    </article>
                    <? include('inc/coluna-mpi.php');?><br class="clear">
                    <? include('inc/busca-mpi.php');?>
                    <? include('inc/form-mpi.php');?>
                    <? include('inc/regioes.php');?>
                </section>
            </div>
        </main>
    </div>
    <? include('inc/footer.php');?>
</body>

</html>