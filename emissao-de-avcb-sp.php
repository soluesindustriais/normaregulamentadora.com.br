<? $h1 = "Emissão de avcb sp"; $title  = "Emissão de avcb sp"; $desc = "Receba diversos orçamentos de $h1, você encontra nos resultados das pesquisas do Soluções Industriais, cote imediatamente com dezenas de fornecedores de todo o Brasil"; $key  = "Emissões de avcb sp,Emissão de avcb"; include('inc/head.php'); include('inc/fancy.php'); ?></head><body><? include('inc/topo.php');?><div class="wrapper"><main><div class="content"><section><?=$caminhoinformacoes?><br class="clear" /><h1><?=$h1?></h1><article><div class="img-mpi"><a href="<?=$url?>imagens/mpi/emissao-de-avcb-sp-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/emissao-de-avcb-sp-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/emissao-de-avcb-sp-02.jpg" title="Emissões de avcb sp" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/emissao-de-avcb-sp-02.jpg" title="Emissões de avcb sp" alt="Emissões de avcb sp"></a><a href="<?=$url?>imagens/mpi/emissao-de-avcb-sp-03.jpg" title="Emissão de avcb" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/emissao-de-avcb-sp-03.jpg" title="Emissão de avcb" alt="Emissão de avcb"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />

<p>O documento AVCB (Auto de Vistoria do Corpo de Bombeiros) é obrigatório para certificar as condições de segurança contra chamas em diversos ambientes por meio de um conjunto de medidas organizacionais, estruturais e técnicas que visam garantir o nível de proteção adequado contra incêndio e pânico.</p>

<p>Na emissão do documento AVCB, estará presente as medidas de segurança contra situações de:</p>

<ul class="topicos-relacionados">
    <li>Incêndio;</li>
    <li>Vasos subterrâneos;</li>
    <li>Presença de gases inflamáveis no local;</li>
    <li>Grupo moto gerador que pode apresentar riscos;</li>
    <li>Más condições das instalações elétricas;</li>
    <li>Entre outros casos.</li>
</ul>

<p>As normas determinam que toda edificação de uso coletivo precisa da vistoria do Corpo de Bombeiros, o AVCB. Sua função é certificar e garantir que o local passou por uma vistoria, e que o mesmo encontra-se em conformidade com as leis vigentes e em condições de segurança contra incêndio.</p>

<p>Após a emissão do AVCB em São Paulo ou em outra região é preciso renová-lo periodicamente segundo a legislação.</p>

<h2>Para que serve o AVCB em São Paulo?</h2>

<p>O AVCB é um documento extremamente importante, ele serve por exemplo para emitir o Alvará de Funcionamento do estabelecimento comercial, contratação de apólice de Seguros e evita que o estabelecimento receba multas por não cumprimento da legislação.</p>

<h2>Como Emitir ou Renovar o AVCB em São Paulo?</h2>

<p>Para emitir ou renovar um AVCB atualizado na cidade de São Paulo é necessário montar um Projeto de Segurança contra incêndios, e enviá-lo ao Corpo de Bombeiros (CBPMESP), que irá dar andamento a requisição.</p>

<p>No Projeto Técnico de Segurança Contra Incêndios, deverá conter todos os elementos e equipamentos de segurança como luzes de emergência, extintores, portas corta fogo, hidrantes, sprinklers, mangueiras de incêndio, etc.</p>

<p>O Corpo de Bombeiros (CBPMESP) irá então vistoriar tanto a documentação do projeto quanto o local, para se certificar de que normas da legislação vigente estejam em conformidade para a segurança dos ocupantes e da estrutura do prédio. </p>

<p>Caso haja alguma irregularidade, o Corpo de Bombeiros apontará as alterações a serem realizadas e irá estipular um novo prazo para a próxima vistoria.</p>


</article><? include('inc/coluna-mpi.php');?><br class="clear"><? include('inc/busca-mpi.php');?><? include('inc/form-mpi.php');?><? include('inc/regioes.php');?></section></div></main></div><? include('inc/footer.php');?></body></html>