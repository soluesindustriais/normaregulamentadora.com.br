<footer>
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="<?=$url?>" title="Página inicial">Home</a></li>
					<li><a href="<?=$url?>informacoes" title="Informacoes">Informações</a></li>
					<li><a href="<?=$url?>sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a href="<?=$url?>mapa-site" title="Mapa do site">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
	 <script>
// Função para pegar o value do primeiro h2 da página
function getFirstH2Value() {
  const h2 = document.querySelector("h2");
  return h2.textContent;
}

// Função para pegar os primeiros 200 caracteres do primeiro <p> após o primeiro h2
function getFirstPValue() {
  const p = document.querySelector("h2 + p");
  const text = p.textContent.slice(0, 200);
  return text + "...";
}

// Criação do objeto de schema inicial
const schema = {
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [
    {
      "@type": "Question",
      "name": getFirstH2Value(),
      "acceptedAnswer": {
        "@type": "Answer",
        "text": getFirstPValue()
      }
    }
  ]
};

// Função para adicionar uma nova pergunta e resposta ao schema inicial
function addNewQuestion() {
  const secondH2 = document.querySelector("h2:nth-of-type(3)");
  const secondP = document.querySelector("h2:nth-of-type(3) + p");
  if (secondH2 && secondP) {
    schema.mainEntity.push({
      "@type": "Question",
      "name": secondH2.textContent,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": secondP.textContent
      }
    });
  }
}

// Chama a função para adicionar uma nova pergunta e resposta, caso exista
addNewQuestion();

// Criação do elemento script no footer com o valor do schema criado
const script = document.createElement("script");
script.type = "application/ld+json";
script.text = JSON.stringify(schema);
document.querySelector("footer").appendChild(script);

</script>
</footer>
<div class="copyright-footer">
	<div class="wrapper-footer">
		Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
		<div class="center-footer">
			<img src="imagens/img-home/logo-footer.png" alt="<?=$nomeSite?>" title="<?=$nomeSite?>">
			<p>é um parceiro</p>
			<img src="imagens/logo-solucs.png" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fab fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fab fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>
<script defer src="<?=$url?>js/geral.js"></script>
<script src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?=$url?>js/organictabs.jquery.js"></script>
		<link rel="stylesheet" href="css/normalize.css" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<!--SweetAlert Modal-->
		<link rel="stylesheet" href="<?=$url?>js/sweetalert/css/sweetalert.css" />
		<script src="<?=$url?>js/sweetalert/js/sweetalert.min.js"></script>
		<!--/SweetAlert Modal-->
		<!--Máscara dos campos-->
		<script src="<?=$url?>js/maskinput.js"></script>
		<script>
			$(function() {
				$('input[name="telefone"]').mask('(99) 99999-9999');
			});

		</script>

	<!-- MENU  MOBILE -->
	<script src="<?=$url?>js/jquery.slicknav.js"></script>

<script src="//code-sa1.jivosite.com/widget/5GVjIoD9Y7" async></script>


<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-BS8X9LHLS5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-BS8X9LHLS5');
</script>
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  async function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-D23WW3S4NC');
</script>


<!-- BOTAO SCROLL -->
<script async src="<?=$url?>js/jquery.scrollUp.min.js"></script>
<script async src="<?=$url?>js/scroll.js"></script>
<!-- /BOTAO SCROLL -->
<script async src="<?=$url?>js/vendor/modernizr-2.6.2.min.js"></script>
<script async src="<?=$url?>js/app.js"></script>

<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>


    <!-- Script Launch start -->
    <script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
    <script>
        const aside = document.querySelector('aside');
        const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
        aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
    </script>
    <!-- Script Launch end -->
<?php include 'inc/fancy.php'; ?><div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>