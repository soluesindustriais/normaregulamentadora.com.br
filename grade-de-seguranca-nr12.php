<? $h1 = "Grade de segurança nr12";
$title  = "Grade de segurança nr12";
$desc = "Orce $h1, conheça as melhores empresas, realize um orçamento agora mesmo com centenas de fabricantes de todo o Brasil";
$key  = "Grades de segurança nr12,segurança nr12";
include('inc/head.php');
include('inc/fancy.php'); ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/grade-de-seguranca-nr12-01.jpg" title="<?= $h1 ?>" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/grade-de-seguranca-nr12-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/grade-de-seguranca-nr12-02.jpg" title="Grades de segurança nr12" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/grade-de-seguranca-nr12-02.jpg" title="Grades de segurança nr12" alt="Grades de segurança nr12"></a><a href="<?= $url ?>imagens/mpi/grade-de-seguranca-nr12-03.jpg" title="segurança nr12" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/grade-de-seguranca-nr12-03.jpg" title="segurança nr12" alt="segurança nr12"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>Como escolher a proteção ideal para a sua máquina dentro das conformidades da NR12? Este é um assunto em constante discussão atualmente, e de certa forma simples de ser entendido e resolvido.</p>
                        <p>O ideal para a elaboração de uma célula robotizada ou linha de produção segura, e que atenda as normas é a instalação de uma proteção física confiável, eficaz e que tenha sua resistência comprovada, desta forma, uma <strong>grade de segurança NR12</strong> instalada de forma correta e padronizada já resolverá boa parte das adequações necessárias para deixar a sua área de risco protegida.</p>
                        <p>Veja também <a target='_blank' title='grades de proteção' href="https://www.normaregulamentadora.com.br/grade-de-protecao-nr12"style='cursor: pointer; color: #006fe6;font-weight:bold;'>grades de proteção</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>
                        <h2>Variedade de opções</h2>
                        <p>As empresas fornecedoras da grade de segurança, oferece também diversas opções para seus clientes, tais como:</p>
                        <ul class="topicos-relacionados">
                            <li>Cercado de segurança para indústria;</li>
                            <li class="li-mpi">Gaiola para máquinas;</li>
                            <li class="li-mpi">Cofre para mídias;</li>
                            <li class="li-mpi">Entre outras.</li>
                        </ul>
                        <h2>Outras informações</h2>
                        <p>Além de ser extremamente eficiente, a <strong>grade de segurança NR12</strong> é um produto modular, os painéis são instalados a uma distância de 150 mm do chão, utiliza como altura padrão dos painéis 2050 mm e estes são instalados juntamente com os postes para fixação.</p>
                        <p>A altura pode ser facilmente ajustada com a utilização do "Kit Cut" (perfil de corte para adaptação horizontal), a largura da grade de proteção tem 07 padrões: 200 mm; 300 mm; 700 mm; 800 mm; 1.000 mm; 1.200mm e 1.500 mm podendo esta ser ajustadas de acordo com suas necessidades e com a utilização do "Kit End" (perfil de corte para adaptação vertical). Ambas as adaptações são feitas sem a necessidade de solda.</p>
                        <p>Pode-se combinar painéis para atingir maiores larguras sem a utilização de postes com o "Snapper" (conector de reforço).</p>
                        <h2>Sobre a empresa</h2>
                        <p>As empresas trabalham com os sistemas Smart Fix, Rapid Fix e Combi Fix, os quais facilitam a instalação, remoção ou substituição de qualquer componente da proteção.</p>
                        <p>As empresas fornecem soluções para proteção de máquinas, robôs e perímetros. Utilizando um sistema de painéis modular, conseguimos atender às mais variadas necessidades de seus clientes.</p>
                        <p>Para obter maiores informações sobre a <strong>grade de segurança NR12</strong>, entre em contato com a empresa. Aproveite para solicitar um orçamento!</p>
                        <p>Todos os produtos possuem norma ISO Standards de qualidade.</p>
                        <p>Faça um orçamento pelo formulário abaixo. É simples, rápido e gratuito!</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>