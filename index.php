<?
$h1         = 'Norma Regulamentadora';
$title      = 'Início - Norma Regulamentadora';
$desc       = 'Norma Regulamentadora - Conte com os melhores empresas de Laudo e NR do Brasil. É gratis!';  
$var        = 'Home';
include('inc/head.php');
include('inc/fancy.php');
?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>LAUDO & NR</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>Emissão de AVCB SP</h2>
        <p>O documento AVCB (Auto de Vistoria do Corpo de Bombeiros) é obrigatório para certificar as condições de segurança contra chamas em diversos ambientes por meio de um conjunto de...</p>
        <a href="<?=$url?>emissao-de-avcb-sp" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Adequação NR 10</h2>
        <p>A adequação tem como principal função ajudar com a segurança dos trabalhadores que lidam com eletricidade nas empresa. Dessa forma, a empresa deve organizar o Prontuário das Instalações Elétricas (PIE). Esse documento é elaborado em formato de um manual, que estabelece o sistema de segurança elétrica da empresa.</p>
        <a href="<?=$url?>adequacao-nr10" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>Inspeção NR 13</h2>
        <p>A inspeção NR 13 é um procedimento executado em empresas que possuem caldeiraria em suas instalações, com o objetivo de analisar as condições de segurança relacionadas ao trabalho do operador da caldeira a vapor, e. devem respeitar algumas normas específicas da legislação.</p>
        <a href="<?=$url?>inspecao-nr-13" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>INSPEÇÃO</h2>
        <div class="div-img">
          <p data-anime="left-0">Toda empresa necessita de um planejamento adequado para prevenir possíveis interrupções e imprevistos, por essa razão é sempre importante contar com uma equipe especializada em solucionar problemas, assim esta tarefa se torna mais corriqueira e menos impactante na linha de produção industrial, esse processo é chamado de manutenção preventiva.</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/laudo-inspecao.jpg" alt="Solda Elétrica" title="Solda Elétrica">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>No caso da manutenção preventiva torre de resfriamento, por exemplo, a manutenção preventiva auxilia em uma série de fatores, como por exemplo:</p>
            <li><i class="fas fa-angle-right"></i>Reduzir riscos de quebra;</li>
            <li><i class="fas fa-angle-right"></i>Prevenir o envelhecimento e degeneração dos equipamentos;</li>
            <li><i class="fas fa-angle-right"></i>Programar a conversação das peças;</li>
            <li><i class="fas fa-angle-right"></i>Amenizar os custos de compra de novos itens.</li>
          </ul>
          <a href="<?=$url?>inspecao-nr-13" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="far fa-file-alt fa-7x"></i>
            <div>
              <p>LAUDO AVCB / CLCB</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-search fa-7x"></i>
            <div>
              <p>INSPEÇÃO NR 10 / 12 / 13</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fas fa-shield-alt fa-7x"></i>
            <div>
              <p>ADEQUAÇÃO NR12</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
       <div class="content-icons">

        <div class="produtos-relacionados-1">
          <figure>
            <div class="fig-img2">
            <a href="<?=$url?>adequacao-nr12">
              <h2>ADEQUAÇÃO NR12</h2>
                </a>
                <a href="<?=$url?>adequacao-nr12">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-2">
          <figure class="figure2">
              <div class="fig-img2">
            <a href="<?=$url?>inspecao-nr-13">
                <h2>INSPEÇÃO NR 13</h2>
            </a>
                <a href="<?=$url?>inspecao-nr-13">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-3">
          <figure>
              <div class="fig-img2">
              <a href="<?=$url?>clcb">
                <h2>CLCB</h2>
                </a>
                <a href="<?=$url?>clcb">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-avcb-e-clcb.jpg" class="lightbox" title="Avcb e clcb">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-avcb-e-clcb.jpg" title="Avcb e clcb" alt="Avcb e clcb">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-auto-de-vistoria-do-corpo-de-bombeiros.jpg" class="lightbox"  title="Auto de vistoria do corpo de bombeiros">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-auto-de-vistoria-do-corpo-de-bombeiros.jpg" alt="Auto de vistoria do corpo de bombeiros" title="Auto de vistoria do corpo de bombeiros">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-adequacao-nr12.jpg" class="lightbox" title="Adequação nr12">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-adequacao-nr12.jpg" alt="Adequação nr12" title="Adequação nr12">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-adequacao-nr1.jpg" class="lightbox" title="Adequação nr1">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-adequacao-nr1.jpg" alt="Adequação nr1" title="Adequação nr1">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empresa-especializada-em-nr-12.jpg" class="lightbox" title="Empresa especializada em nr 12">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empresa-especializada-em-nr-12.jpg" alt="Empresa especializada em nr 12"  title="Empresa especializada em nr 12">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-inspecao-nr-13.jpg" class="lightbox" title="Inspeção nr 13">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-inspecao-nr-13.jpg" alt="Inspeção nr 13" title="Inspeção nr 13">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-protecao-de-maquinas-e-equipamentos-rotativos.jpg" class="lightbox" title="Proteção de máquinas e equipamentos rotativos">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-protecao-de-maquinas-e-equipamentos-rotativos.jpg" alt="Proteção de máquinas e equipamentos rotativos" title="Proteção de máquinas e equipamentos rotativos">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-laudo-avcb.jpg" class="lightbox" title="Laudo avcb">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-laudo-avcb.jpg" alt="Laudo avcb" title="Laudo avcb">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-emissao-de-avcb-sp.jpg" class="lightbox" title="Emissão de avcb sp">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-emissao-de-avcb-sp.jpg" alt="Emissão de avcb sp" title="Emissão de avcb sp">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-adequacao-de-prensas-nr12.jpg" class="lightbox" title="Adequação de prensas nr12">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-adequacao-de-prensas-nr12.jpg" alt="Adequação de prensas nr12" title="Adequação de prensas nr12">
              </a>
              </li>
          </ul>
        </div>
      </div>
      <div class="bg-cinza">
        <div class="wrapper">
          <h2 class="txtcenter h2black">Saiba mais sobre AVCB</h2>
          <ul class="thumbnails-mod1">
            <li>
              <a href="<?=$url;?>laudo-avcb" title="LAUDO AVCB"><img src="<?=$url;?>imagens/mpi/laudo-avcb-01.jpg" alt="LAUDO AVCB" title="LAUDO AVCB"/></a>
              <h2><a href="<?=$url;?>laudo-avcb" title="LAUDO AVCB">LAUDO AVCB</a></h2>
            </li>
            <li>
              <a href="<?=$url;?>avcb-bombeiros" title="AVCB BOMBEIROS"><img src="<?=$url;?>imagens/mpi/avcb-bombeiros-01.jpg" alt="AVCB BOMBEIROS" title="AVCB BOMBEIROS"/></a>
              <h2><a href="<?=$url;?>avcb-bombeiros" title="AVCB BOMBEIROS">AVCB BOMBEIROS</a></h2>
            </li>
            <li>
              <a href="<?=$url;?>consultoria-para-avcb" title="CONSULTORIA PARA AVCB"><img src="<?=$url;?>imagens/mpi/consultoria-para-avcb-01.jpg" alt="CONSULTORIA PARA AVCB" title="CONSULTORIA PARA AVCB"/></a>
              <h2><a href="<?=$url;?>consultoria-para-avcb" title="CONSULTORIA PARA AVCB">CONSULTORIA PARA AVCB</a></h2>
            </li>
            <li>
              <a href="<?=$url;?>obtencao-de-avcb" title="OBTENÇÃO DE AVCB"><img src="<?=$url;?>imagens/mpi/obtencao-de-avcb-01.jpg" alt="OBTENÇÃO DE AVCB" title="OBTENÇÃO DE AVCB"/></a>
              <h2><a href="<?=$url;?>obtencao-de-avcb" title="OBTENÇÃO DE AVCB">OBTENÇÃO DE AVCB</a></h2>
            </li>
          </ul>
        </div>
      </div>
    </section>
</main>
<?php include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>