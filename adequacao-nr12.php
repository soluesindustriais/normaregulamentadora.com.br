<? $h1 = "Adequação nr12";
$title  = "Adequação nr12";
$desc = "Se pesquisa por $h1, ache os melhores fabricantes, solicite diversos comparativos pela internet com dezenas de fábricas ao mesmo tempo";
$key  = "Adequações nr12,Empresa de adequação nr12";
include('inc/head.php');
include('inc/fancy.php'); ?>
</head>
<body>
<? include('inc/topo.php');?>
<div class="wrapper">
	<main>
		<div class="content">
			<section><?=$caminhoinformacoes?><br class="clear" />
				<h1><?=$h1?></h1>
				<article>
					<div class="img-mpi">
						<a href="<?=$url?>imagens/mpi/adequacao-nr12-01.jpg" title="<?=$h1?>" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/adequacao-nr12-01.jpg" title="<?=$h1?>" alt="<?=$h1?>"></a><a href="<?=$url?>imagens/mpi/adequacao-nr12-02.jpg" title="Adequações nr12" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/adequacao-nr12-02.jpg" title="Adequações nr12" alt="Adequações nr12"></a><a href="<?=$url?>imagens/mpi/adequacao-nr12-03.jpg" title="Empresa de adequação nr12" class="lightbox"><img src="<?=$url?>imagens/mpi/thumbs/adequacao-nr12-03.jpg" title="Empresa de adequação nr12" alt="Empresa de adequação nr12"></a>
						</div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span><hr />
						<h2>ADEQUAÇÃO NR12 PARA EQUIPAMENTOS INDUSTRIAIS</h2>
						<p>Existem diversas normas regulamentadoras para o funcionamento das empresas e essas normas têm como objetivo fazer com que as empresas funcionem da melhor forma  seguindo um padrão e priorizando a segurança do local, funcionários e sociedade.</p>
						<p>Dentre tantas normas, existe <strong>Adequação nr12</strong> norma estabelecida pelo Ministério do trabalho a qual estabelece diversos conceitos e exigências para o perfeito funcionamento de máquinas e equipamentos industriais a fim de padronizar e estabelecer maior segurança.</p>
						<p>Desde o início da norma em 1978, já foram feitas diversas alterações, pois foram observados a necessidade de ajustar diversos pontos mediante ao desenvolvimento e crescimento da indústria. A última alteração, ainda mais rigorosa determinou obrigações igualitárias para fabricantes e usuários quanto a responsabilidade do funcionamento dos equipamentos.</p>
						<p>Além da <strong>Adequação nr12</strong> ser exigida para a operação das máquinas nas indústria, também existem exigências específicas para os fabricantes englobando desde a fabricação até a operação para <strong>adequação nr12</strong> pensando em um padrão.De forma geral, veja alguns pontos que são abordados na norma:</p>
						<ul class="topicos-relacionados">
							<li>Armazenamento dos equipamentos;</li>
							<li>Modo de instalação e operação;</li>
							<li>Equipamentos de proteção;</li>
							<li>Orientações para prevenção de acidentes;</li>
							<li>Orientações para preservação da saúde dos trabalhadores.</li>
						</ul>
						<p>Esses são os principais pontos mencionados na norma, mas existem muitos outros, porém varia muito devido ao tipo de equipamento, pois cada um possui funcionalidades e métodos diferentes, por isso o fabricante e o cliente precisa estar atento a essa especificação a fim da <strong>Adequação nr12</strong> do seu equipamento atender as exigências.</p>
						
						<p>Veja também <a target='_blank' title='grades de proteção' href="https://www.normaregulamentadora.com.br/grade-de-protecao-nr12"style='cursor: pointer; color: #006fe6;font-weight:bold;'>grades de proteção</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>
						
						<h2>QUAIS MEDIDAS SÃO ESTABELECIDAS NA NORMA?</h2>
						<p>Como vimos nos tópicos acima, a <strong>Adequação nr12</strong> tem como foco definir medidas preventivas, estabelecer métodos de operação e proteção e assim diminuir cada vez mais os riscos de acidentes de trabalhos ou falhas durante o funcionamento do equipamento.</p>
						<p>A norma de <strong>Adequação nr12</strong> estabelece padrões de como e onde instalar determinadas máquinas, considerando qual a distância que devem ter de outro equipamentos, determina a estrutura como pisos mais resistentes para equipamentos mais pesados, ambientes limpos e refrigerados.</p>
						<p>Para instalações elétricas, estabelece métodos para que os equipamentos dependentes de rede elétrica sejam instalados com mais segurança a fim de evitar situações de risco como incêndios, explosões, choques, entre outro acidentes proveniente de uma  fabricação ou instalação incorreta. Além disso, exige que quadros elétricos sejam sinalizados e tenham as portas e acesso sempre fechadas para que não fiquem expostos e com isso possam sofrer alguma intervenção externa ou danificação de cabeamento, entre outras situações.</p>
						<p>Os equipamentos devem possuir um manual de uso fornecido pelo fabricante explicando em como operar determinado equipamento considerando a <strong>Adequação nr12</strong>. No manual deve conter como o equipamento funciona, quais os risos e orientações para combater ou agir em situações de falhas ou riscos e quais os melhores métodos para proteção o operador.</p>
						<h2>QUAIS CUIDADOS TER AO COMPRAR UM EQUIPAMENTO ?</h2>
						<p>É importante conferir se o equipamento é legal e se o fabricante do equipamento possui nota fiscal e oferece garantia para o produto. Além disso, é preciso identificar se a empresa fabricante atende as normas nr12 e por isso ao comprar um equipamento é importante que o comprador avalie todos esse aspectos e que entenda como o equipamento funciona e quais as normas estabelecidas para determinado equipamento.</p>
						<p>Ainda mais para as empresas, é imprescindível que a nota fiscal seja guardada a fim de comprovar a compra e em casos e falhas no funcionamento do equipamento o comprador tenha seus direitos garantidos.</p>
						<h2>QUAIS SÃO OS DOCUMENTOS MÍNIMOS PARA ATENDER A NORMA?</h2>
						<p>Existem muitos documentos fundamentais que precisam ser elaborado principalmente para os casos e industrias, fabricas e empresas que possuem número maior de equipamentos que estão dentro da exigência da <strong>Adequação nr12</strong>. Veja abaixo algumas documentações fundamentais para providenciar:</p>
						<h2>1- INVENTÁRIO DE MÁQUINAS</h2>
						<p>O primeiro passo é fazer um levantamento de todos os equipamentos existentes no local, mencionando algumas informações importantes como nome do equipamento, localização e algumas informações técnicas como potência e capacidade.</p>
						<p>Este documento tem como objetivo centralizar informações e permite melhor visualização de todos os equipamentos existentes e assim conseguir identificar quais os riscos que oferecem para então definir qual o plano de ação para redução dos riscos.</p>
						<h2>2- REPRESENTAÇÃO ESQUEMÁTICA</h2>
						<p>Podemos considerar essa representação como uma planta que aponta a posição das máquinas e os operadores, podendo acrescentar algumas informações complementares como o fluxo do processo.</p>
						<p>Essa representação permite a identificação da localização das máquinas sem a necessidade de uma visualização presencial e com isso permite mais agilidade para identificação de processos e riscos.</p>
						<h2>3- ANÁLISE DE RISCO</h2>
						<p>Nesse procedimento todos os riscos oferecidos pelas máquinas são mapeados, sendo considerado  um dos principais, pois a partir dele é possível visualizar os reais riscos e permite pensar em um plano de ação para uma solução assertiva e mais segura.</p>
						<p>A Análise de risco é exigida para <strong>Adequação nr12</strong>, porém ainda não é o plano de ação, pois se trata apenas do levantamento para saber quais são os riscos oferecidos, mas se atendem a norma nr12 somente no próximo item é possível ter essa definição.</p>
						<h2>4- DIAGNÓSTICO</h2>
						<p>Esse item considera a norma nr12 e vai avaliar se todo o levantamento feito no itens anteriores atendem ou não a norma, sendo em alguns casos, necessário a <strong>Adequação nr12</strong>.</p>
						<p>Existe uma diferença entre a análise de risco e a <strong>Adequação nr12</strong>, sendo que a análise de risco indica a necessidade de proteção mediante ao risco identificado, já a <strong>Adequação nr12</strong> indica em como agir para a proteção mediante aos riscos identificados.</p>
						<h2>5- PLANO DE AÇÃO</h2>
						<p>Após todos os levantamentos feitos o ideal é providenciar a regularização para o funcionamento das máquinas atendendo a <strong>adequação nr12</strong>. Quanto mais as normas forem atendidas menores serão os riscos de acidentes e falhas durante a operação o equipamento.</p>
						<h2>6- MANUAIS</h2>
						<p>Os equipamentos devem ser acompanhados de um manual de operação e manutenção. O manual deve conter todas as informações de como utilizar o equipamento e como agir em casos de falhas.</p>
						<p>O plano de ação é um dos principais entre todos e é nesse processo que os riscos serão reduzidos de fato, gerando resultados positivos para empresas, indústrias ou fábricas. Para entender melhor como esse processo é constituído veja abaixo alguns pontos que fazem parte desse processo:</p>
						<ul class="topicos-relacionados">
							<li>Identificação o perigo ou risco;</li>
							<li>Estimativa dos riscos, quais são as consequências e prejuízos;</li>
							<li>Elaboração de melhores medidas para redução de riscos;</li>
							<li>Aplicação de medidas baseadas na Adequação nr12;</li>
							<li>Avaliação da redução dos riscos.</li>
						</ul>
						<h2>QUAIS OS PRINCÍPIOS BÁSICOS PARA O SISTEMA DE SEGURANÇA?</h2>
						<p>Todo sistema de segurança entende que a fabricação correta de produtos ou máquinas é o ponto focal para que todo o processo seja bem sucedido, pois diminui as chances de falhas e riscos. Sendo assim, garantindo uma fabricação e instalação considerando a <strong>Adequação nr12</strong> é possível reduzir consequências negativas pelo uso.</p>
						<p>Para isso é fundamental que no ato da fabricação e instalação a normas sejam levantadas e consideradas. Um ponto muito importante é esse processo ser conduzido por um responsável técnico que vai garantir funcionalidade.</p>
						<p>Além disso, existem também as normas que são exigidas pelo Ministério do trabalho que oferecem aos funcionários mais segurança, evitando acidentes de trabalho e também problemas de saúde relacionados ao trabalho.</p>
						<h2>COMO FUNCIONA A CAPACITAÇÃO E ACORDO COM A NORMA?</h2>
						<p>De acordo com a <strong>Adequação nr12</strong>, o funcionário deve iniciar um treinamento antes de assumir a operação das máquinas, é fundamental que o empregador ofereça um  treinamento e que teste a capacidade o funcionário em exercer determinada função, pois se não estiver apto as chances de um acidente e trabalho acontecer só aumentam.</p>
						<p>Além dos cuidados para aquisição das máquinas é fundamental que o empregador tenha cuidado também na formação de seus funcionários e garanta que estejam aptos a conduzir os equipamentos. Outra ação muito importante é oferecer cursos de reciclagem, onde os funcionários serão atualizados sobre novas tecnologias e até mesmo de informações sobre o procedimento que foram esquecidas.</p>
						<h2>QUAIS SÃO OS BENEFÍCIOS PARA O TRABALHADOR?</h2>
						<p>A norma foi criada com o fundamental de preservar a saúde  e integridade física do trabalhador e para isso existe o Ministério Público do Trabalho exige que algumas normas sejam seguidas.</p>
						<p>Uma das exigências é que o empregador assume os custos do afastamento do funcionário em casos de acidentes de trabalho ou doenças comprovadas por um médico que são de origem trabalhista.</p>
						<p>A <strong>Adequação nr12</strong> não estabelece apenas os deveres do empregador com o funcionário, mas também do funcionário com o maquinário, pois é necessário que o funcionário seja prudente ao conduzir o processo, pois não adianta a empresa oferecer equipamentos de segurança e o funcionário não utilizar e nesse caso ocorrer um acidente por falta de equipamento de proteção, por isso é importante ter a participação e compromisso de ambos quando o assunto é segurança, embora a responsabilidade do funcionário não ausente a responsabilidade do empregador em casos de acidentes.</p>
						<p>É muito importante que os funcionários e os empregadores estejam cientes da <strong>Adequação nr12</strong> para aplicar no dia a dia, por isso, é aconselhável que os empregadores sempre contratem um profissional apto para aplicar o curso de manutenção e  que nesse curso as normas de segurança e trabalho sejam discutidas e atualizadas.</p>
						<h2>QUAIS SÃO AS MEDIDAS DE PROTEÇÃO?</h2>
						<p>Existem três tipos de medidas protetivas que podem ser aplicadas para garantir mais segurança para a integridade dos equipamentos e funcionários da empresa. veja abaixo quais são:</p>
						<h2>MEDIDA COLETIVAS</h2>
						<p>Ocorre quando um ser humano está em uma zona de risco durante o funcionamento da máquina e para isso a norma exige que o maquinário possua sensores para detectar a presença humana e além disso ofereça algum recurso manual para controle do maquinário em situações de emergência.</p>
						<h2>MEDIDAS  ADMINISTRATIVAS</h2>
						<p>Nessa medida é observada se a máquina e os equipamentos atendem algumas exigências da norma, como se a máquina possui distância correta, pisos e estruturas reforçadas e toda a armazenagem dos materiais, de forma correta e segura.</p>
						<h2>MEDIDAS INDIVIDUAIS</h2>
						<p>Nessas medidas constam os Equipamentos de Proteção Individual conhecidos como EPIs,  são luvas, respiradores, capacetes, cintos de segurança, óculos, macacão anti chama,  entre outros equipamentos que oferecem ao funcionário mais segurança e proteção de forma individual.</p>
						<h2>QUEM É RESPONSÁVEL PELA FISCALIZAÇÃO?</h2>
						<p>O ministério do trabalho e emprego é responsável pela fiscalização de empresas, indústrias e fábricas que possuem maquinários. As empresas recebem uma vistoria pelos profissionais técnicos do órgão a fim de identificar se estão cumprindo com as exigências previstas na <strong>Adequação nr12</strong>.</p>
						<h2>A NORMA É APLICADA EM TODAS AS EMPRESAS?</h2>
						<p>Nem todas as empresas conseguem se adequar às normas de <strong>Adequação nr12</strong>, existe uma grande dificuldade ainda mais quando a empresa já existe e possui seus maquinários e tenta a <strong>adequação nr12</strong> após, por isso, é mais simples quando a empresa inicia suas atividade já considerando essas normas antes da compra e instalação das máquinas.</p>
						<p>É muito importante que a empresa tenha supervisores para acompanhar os processos e garantir que os operadores estão conduzindo o maquinário de forma correta e com isso reduzir os riscos de acidentes.</p>
						<h2>QUAIS SÃO OS RISCOS AO DESCUMPRIR AS NORMAS?</h2>
						<p>Existem riscos para desfavoráveis para a empresa e também para os funcionários como acidentes que podem lesar uma região do corpo ou até mesmo provocar a morte, além de prejudicar a imagem da empresa no mercado, aumento do risco de novos acidentes, pagamento de multas e indenizações comuns e trabalhistas ao colaborador caso ocorra algum acidente.</p>
						<p>O não cumprrimento da <strong>Adequação nr12</strong> pode provocar muita insegurança aos funcionários que não estarão seguros ao desenvolver suas atividades e não terão equipamentos suficientes para a operação segura.</p>
						<h2>QUAIS AS VANTAGENS EM ADERIR A ADEQUAÇÃO NR12?</h2>
						<p>São inúmeras vantagens para as empresas e funcionários, pois além de garantir o funcionamento dos maquinário de forma correta garante também a saúde do funcionários evitando custos com indenizações e afastamentos que podem ser evitados com as medidas preventivas.</p>
						<p>Sendo assim, o ideal que a empresa antes de iniciar as atividades contrate um profissional técnico para levantar as medidas de segurança necessárias para <strong>adequação nr12</strong>. Com isso, será possível comprar equipamentos com qualidade e garantir uma instalação e operação sem falhas e com menor risco a saúde.</p>
					</article>
					<? include('inc/coluna-mpi.php');?>
					<br class="clear">
					<? include('inc/busca-mpi.php');?>
					<? include('inc/form-mpi.php');?>
					<? include('inc/regioes.php');?>
				</section>
			</div>
		</main>
	</div>
	<? include('inc/footer.php');?>
</body>
</html>